﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using StringHelper;

namespace UseClass
{
    internal static class Program
    {
        /* Zadania: 
         * 1. Dodać możliwość dodawania, odejmowania, mnożenia i dzielenia             +
         * 2. Zmienne a i b pobierać z klawiatury           +
         * 3. Za pomocą struktury programistycznej switch -> case, zapytać jakie działanie chce wykonać i takie wykonać  -
         * 4. Napisać nową klase StringHelper do łączenia ciągów znaków.   -
        */

        private static void Main(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());


            var calculator = new Calculator();
            var result = calculator.Addition(a, b);
            var resultSub = calculator.Subtraction(a, b);
            var resultMulti = calculator.Multiplication(a, b);
            var resultDivide = calculator.Divide(a, b);

            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}