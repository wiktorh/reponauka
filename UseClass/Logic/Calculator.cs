﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Calculator
    {
        public int Addition(int a, int b)
        {
            var result = a + b;
            return result;
        }

        public int Subtraction(int a, int b)
        {
            var resultSub = a - b;
            return resultSub;
        }
        
        public double Multiplication(double a, double b)
        {
            var resultMulti = a * b;
            return resultMulti;
        }
        
        public double Divide(double a, double b)
        {
            var resultDivide = a / b;
            return resultDivide;
        }
    }
}
